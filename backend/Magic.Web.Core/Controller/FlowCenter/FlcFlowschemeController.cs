﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.FlowCenter.Service;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 流程管理服务
/// </summary>
[ApiDescriptionSettings("FlowCenter", Name = "FlcFlowscheme", Order = 100, Tag = "流程管理服务")]
public class FlcFlowschemeController : IDynamicApiController
{
    private readonly IFlcFlowschemeService _service;
    public FlcFlowschemeController(IFlcFlowschemeService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询流程管理
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/flcFlowscheme/page")]
    public async Task<PageList<FlcFlowschemeOutput>> Page([FromQuery] QueryFlcFlowschemePageInput input)
    {
        return await _service.Page(input);
    }

    /// <summary>
    /// 增加流程管理
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/flcFlowscheme/add")]
    public async Task Add(AddFlcFlowschemeInput input)
    {
        await _service.Add(input);
    }

    /// <summary>
    /// 删除流程管理
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/flcFlowscheme/delete")]
    public async Task Delete(PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新流程管理
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/flcFlowscheme/edit")]
    public async Task Update(EditFlcFlowschemeInput input)
    {
        await _service.Update(input);
    }

    /// <summary>
    /// 获取流程管理
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/flcFlowscheme/detail")]
    public async Task<FlcFlowschemeOutput> Get([FromQuery] PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }

    /// <summary>
    /// 获取流程管理列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/flcFlowscheme/list")]
    public async Task<List<FlcFlowschemeOutput>> List([FromQuery] QueryFlcFlowschemePageInput input)
    {
        return await _service.List(input);
    }
}
