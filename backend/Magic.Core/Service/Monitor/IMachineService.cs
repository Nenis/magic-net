﻿using Furion.DependencyInjection;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface IMachineService : ITransient
{
    Task<dynamic> GetMachineBaseInfo();
    Task<dynamic> GetMachineNetWorkInfo();
    Task<dynamic> GetMachineUseInfo();
}
