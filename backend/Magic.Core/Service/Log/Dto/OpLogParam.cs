﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Core.Service;

/// <summary>
/// 请求日志参数
/// </summary>
public class QueryOpLogPageInput : PageParamBase
{
    /// <summary>
    /// 名称
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 是否执行成功（Y-是，N-否）
    /// </summary>
    public YesOrNot Success { get; set; }

    /// <summary>
    /// 请求方式（GET POST PUT DELETE)
    /// </summary>
    public string ReqMethod { get; set; }

}
