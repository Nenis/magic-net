﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysOnlineUserService : ITransient
{
    Task<dynamic> List(PageParamBase input);

    Task ForceExist(OnlineUser user);

    Task PushNotice(SysNotice notice, List<long> userIds);
}
