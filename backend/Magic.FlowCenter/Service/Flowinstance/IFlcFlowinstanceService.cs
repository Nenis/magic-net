﻿using Furion.DependencyInjection;
using Magic.Core;
using Magic.FlowCenter.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace Magic.FlowCenter.Service;

	public interface IFlcFlowinstanceService: ITransient
{
    Task Add(FlcFlowinstance input);
    Task Delete(PrimaryKeyParam input);
    Task Cancel(PrimaryKeyParam input);
    Task Verification(Verification input);
    Task<List<FlcFlowinstance>> List(QueryFlcFlowinstancePageInput input);
    Task<PageList<FlcFlowinstance>> Page(QueryFlcFlowinstancePageInput input);
    Task Update(FlcFlowinstance input);
    Task<List<FlcFlowInstanceOperationHistory>> QueryHistories(PrimaryKeyParam input);
    Task<FlcFlowinstanceOutput> GetForVerification(PrimaryKeyParam input);

    Task<FlcFlowinstanceOutput> Get(long id);
}
